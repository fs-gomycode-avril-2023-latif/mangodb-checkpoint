const mongoose = require('mongoose')
const {Schema} = mongoose

const productSchema = Schema ({
    nom : {type : String, required: true, default:'riz'},
    description : {type : String},
    photo : {type : String},
    price : {type : Number},
    quantity : {type : Number},
    preferences : [],
    colors : [{variant : String, color : String}],
    address : {rue : String}
})

const productModel = mongoose.model('Product',productSchema)

module.exports = productModel