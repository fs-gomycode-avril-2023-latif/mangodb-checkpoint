const mongoose = require('mongoose')
const {Schema}= mongoose

const categorySchema = Schema({
    nom : {type:String}
})

const categoryModel = mongoose.model('Category',categorySchema)

module.exports = categoryModel