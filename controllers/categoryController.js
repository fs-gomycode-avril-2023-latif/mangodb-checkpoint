const categoryService = require('../services/categoryService')

const index = async (req,res) =>{
    try {
        const data = await categoryService.index(res)
        res.status(200).json(data)
    } catch (error) {
        res.status(error.statut).json({error: error.message})
    }
}

const find = async (req,res) =>{
    try {

        const data = await categoryService.find(req.params.id, res)
        res.status(200).json(data)
    } catch (error) {
        res.status(error.statut).json({error: error.message})
    }
}

const store = async (req,res) =>{
    try {
        const data = await categoryService.store(req.body, res)
        res.status(200).json(data)
    } catch (error) {
        res.status(error.statut).json({error: error.message})
    }
}

const update = async (req,res) =>{
    try {
        const data = await categoryService.update(req.params.id, req.body, res)
        res.status(200).json(data)
    } catch (error) {
        res.status(500).json({error: error.message})
    }
}

const destroy = async (req,res) =>{
    try {
        const data = await categoryService.destroy(req.params.id, res)
        res.status(200).json(data)
    } catch (error) {
        res.statut(error.statut).json({error: error.message})
    }
}

module.exports = {index,find,store,update,destroy}