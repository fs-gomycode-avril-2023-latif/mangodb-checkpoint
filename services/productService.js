const productModel = require("../models/productModel")

// ajouté
const store = async (body,res)=>{
    try {
        const productData = new productModel(body);
        const data = await productData.save()
        return data
    } catch (error) {
        res.status(error.statut).json({error: error.message})
    }
}

// liste
const index = async (res)=>{
    try {
        const data = await productModel.find();
        return data
    } catch (error) {
        res.status(error.statut).json({error: error.message})
    }
}

// selectionné 
const find = async (id,res)=>{
    try {

        const data = await productModel.findById(id);
        return data
    } catch (error) {
        res.status(error.statut).json({error: error.message})
    }
}


// modifié 
const update = async (id, body, res)=>{
    try {
        await productModel.findByIdAndUpdate(id,body);
        return true
    } catch (error) {
        res.status(403).json({error: error.message})
    }
}

// supprimé
const destroy = async (id,res)=>{
    try {
        await productModel.findByIdAndDelete(id);
        return true
    } catch (error) {
        res.status(error.statut).json({error: error.message})
    }
}

module.exports = {index,find,update,store,destroy} 