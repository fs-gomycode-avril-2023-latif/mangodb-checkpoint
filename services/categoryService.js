const categoryModel = require("../models/categoryModel")

// ajouté
const store = async (body,res)=>{
    try {
        const categoryData = new categoryModel(body);
        const data = await categoryData.save()
        return data
    } catch (error) {
        res.status(error.statut).json({error: error.message})
    }
}

// liste
const index = async (res)=>{
    try {
        const data = await categoryModel.find();
        return data
    } catch (error) {
        res.status(error.statut).json({error: error.message})
    }
}

// selectionné 
const find = async (id,res)=>{
    try {

        const data = await categoryModel.findById(id);
        return data
    } catch (error) {
        res.status(error.statut).json({error: error.message})
    }
}


// modifié 
const update = async (id, body, res)=>{
    try {
        await categoryModel.findByIdAndUpdate(id,body);
        return true
    } catch (error) {
        res.status(403).json({error: error.message})
    }
}

// supprimé
const destroy = async (id,res)=>{
    try {
        await categoryModel.findByIdAndDelete(id);
        return true
    } catch (error) {
        res.status(error.statut).json({error: error.message})
    }
}

module.exports = {index,find,update,store,destroy} 