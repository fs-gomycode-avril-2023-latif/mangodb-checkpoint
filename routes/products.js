var express = require("express");
var router = express.Router();
const productController = require('../controllers/productController')


/* GET users listing. */
router.get("/", productController.index);
router.get("/:id", productController.find);
router.post("/", productController.store);
router.put("/:id", productController.update);
router.delete("/:id", productController.destroy);



module.exports = router;