var express = require("express");
var router = express.Router();
const categoryController = require('../controllers/categoryController')


/* GET users listing. */
router.get("/", categoryController.index);
router.get("/:id", categoryController.find);
router.post("/", categoryController.store);
router.put("/:id", categoryController.update);
router.delete("/:id", categoryController.destroy);



module.exports = router;