var express = require("express");
var router = express.Router();

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("services");
});

router.get("/text", function (req, res, next) {
  res.render("client", { title: "client" });
});

module.exports = router;
